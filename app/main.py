import wolframalpha

from flask import Flask, request
from flask.views import MethodView

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

class Request(MethodView):
    def post(self):
        outcome = request.json['outcome']
        answer = "I don't know what you are looking for" #DEFAULT
        if 'entities' in outcome:
            if 'wolfram_search_query' in outcome['entities']:
                if len(outcome['entities']['wolfram_search_query']) > 0:
                    if 'value' in outcome['entities']['wolfram_search_query'][0]:
                        query = outcome['entities']['wolfram_search_query'][0]['value']
                        client = wolframalpha.Client('E58HVV-YYKR2QT9HX')
                        response = client.query(query)

                        if len(response.pods) > 0:
                            texts = ""
                            pod = response.pods[1]
                            if pod.text:
                                answer=pod.text
                            else:
                                answer="Cannot render answer"
                        if len(answer) > 140:
                            answer=answer[:140]
                            answer=answer.rsplit(' ', 1)[0]

        ##FILTERING
        answer = answer.replace('km  (kilometers)', 'km')
        answer = answer.replace(' m/s ', ' ')
        print("WOLFRAM ANSWER : " + answer)
        return answer

if __name__ == '__main__':
    app.add_url_rule('/', view_func=Request.as_view('request'))
    app.run(host='0.0.0.0', port=80)
