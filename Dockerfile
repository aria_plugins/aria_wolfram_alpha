FROM aria/python:3.4-onbuild
MAINTAINER A.R.I.A <team@a-r-i-a.co>

RUN echo python main.py > /aria/scripts/run
